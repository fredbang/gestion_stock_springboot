package org.fredcompagny.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ControlleurTest {

	@GetMapping("/test")
	public String test() {
		
		return "test.jsp";
	}
}
