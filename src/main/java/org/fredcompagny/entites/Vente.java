package org.fredcompagny.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Vente implements Serializable{
	

	@Id @GeneratedValue
	private Long idVente;
	private String code;
	@Temporal(TemporalType.TIMESTAMP)
	private Date datevente;
	@OneToMany(mappedBy = "vente")
	private List<LigneVente> ligneVentes;
}
