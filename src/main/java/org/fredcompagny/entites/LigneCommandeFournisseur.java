package org.fredcompagny.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeFournisseur  implements Serializable{
	
	@Id @GeneratedValue
	private Long idLgnecdte;
	@ManyToOne
	@JoinColumn(name = "idarticle")
	private Article article;
	@ManyToOne
	@JoinColumn(name = "idCdtefrnsr")
	private CommandeFournisseur commandeFournisseur;

}
