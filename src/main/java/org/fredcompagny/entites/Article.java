package org.fredcompagny.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "article")
public class Article implements Serializable {
	

	@Id
	@GeneratedValue
	private Long idarticle;
	private String codeArticle;
	private String designation;
	private BigDecimal tauxTva;
	private BigDecimal prixUnitaireTTC;
	private String photo;
	@ManyToOne
	@JoinColumn(name = "idcategory")
	private Category category;
	
	
	public Article() {
		super();
	}
	
	
	public Article(Long idarticle, String codeArticle, String designation, BigDecimal tauxTva,
			BigDecimal prixUnitaireTTC, Category category) {
		super();
		this.idarticle = idarticle;
		this.codeArticle = codeArticle;
		this.designation = designation;
		this.tauxTva = tauxTva;
		this.prixUnitaireTTC = prixUnitaireTTC;	
		this.category = category;
	}
	
	
	public Long getIdarticle() {
		return idarticle;
	}
	public void setIdarticle(Long idarticle) {
		this.idarticle = idarticle;
	}
	public String getCodeArticle() {
		return codeArticle;
	}
	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public BigDecimal getTauxTva() {
		return tauxTva;
	}
	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}
	public BigDecimal getPrixUnitaireTTC() {
		return prixUnitaireTTC;
	}
	public void setPrixUnitaireTTC(BigDecimal prixUnitaireTTC) {
		this.prixUnitaireTTC = prixUnitaireTTC;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
	
	
}
