package org.fredcompagny.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Category implements Serializable {
	
	@Id @GeneratedValue
	private Long idcategory;
	private String code;
	private String designation;
	@OneToMany(mappedBy = "category")
	private List<Article> article;
	
	
	
	public Category() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Category(Long idcategory, String code, String designation) {
		super();
		this.idcategory = idcategory;
		this.code = code;
		this.designation = designation;
	}



	public Long getIdcategory() {
		return idcategory;
	}



	public void setIdcategory(Long idcategory) {
		this.idcategory = idcategory;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public String getDesignation() {
		return designation;
	}



	public void setDesignation(String designation) {
		this.designation = designation;
	}



	public List<Article> getArticle() {
		return article;
	}



	public void setArticle(List<Article> article) {
		this.article = article;
	}

	
	
}
