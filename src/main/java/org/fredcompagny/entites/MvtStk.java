package org.fredcompagny.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class MvtStk  implements Serializable{
	
	public static int Entree = 1;
	public static int sortie =2;
	
	@Id @GeneratedValue
	private Long id;
	private BigDecimal quantite;
	private int typeMvt;
	
	@ManyToOne
	@JoinColumn(name = "article")
	private Article article;

	public MvtStk() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

}
